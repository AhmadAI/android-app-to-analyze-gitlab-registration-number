package com.example.practice1;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.practice1.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button button;
        EditText etext1;
        TextView text3;

        button=findViewById(R.id.button);
        etext1=findViewById(R.id.etext1);
        text3=findViewById(R.id.text3);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String reg = etext1.getText().toString();

                text3.setText("Program: "+"\n"+getProgTitle(reg.substring(0,3))
                        +"\nBatch no.: "+"\nMonths-"+getmonths(reg.substring(3,5))+"\nYear-"+getYear(reg.substring(5,9))+
                        "\nSection-"+getSection(reg.substring(9,11))+"\nSerial no.:"+"\n"+reg.substring(11,13));
                
            }
        });

    }
    private String getProgTitle(String code){
        String title="";

        switch (code)
        {
            case "CSP":
                title = "Certificate in scratch programming";
                break;

            case "CAD":
                title = "Certificate in Android development";
                break;

            case "CPM":
                title = "Certificate in PHP my SQL";
                break;

            case "CJD":
                title = "Certificate in Javascript development";
                break;

            case "CAR":
                title = "Certificate in Arduino programming";
                break;

            case "CDD":
                title = "Certificate in Database design";
                break;

            case "CWD":
                title = "Certificate in Web design";
                break;

            case "CJP":

                title = "Certificate in Java programming";
                break;

            case "CPP":
                title = "Certificate in Python programming";
                break;

            case "CCP":
                title = "Certificate in C sharp programming";
                break;

            case "CVB":
                title = "Certificate in visual basic programming";
                break;

            default:
                title = "Invalid program!";
                break;
        }

        return title;
    }
    private String getmonths (String code){
        String month = "";
        switch(code){
            case "01":
                month = "January";
                break;
            case "02":
                month = "February";
                break;
            case "03":
                month = "March";
                break;
            case "04":
                month = "April";
                break;
            case "05":
                month = "May";
                break;
            case "06":
                month = "June";
                break;
            case "07":
                month = "July";
                break;
            case "08":
                month = "August";
                break;
            case "09":
                month = "September";
                break;
            case "10":
                month = "October";
                break;
            case "11":
                month = "November";
                break;
            case "12":
                month = "December";
                break;
            default:
                month = "invalid month";
                break;
        }

        return month;
    }
    private String getYear(String code){
        String year = "";
        switch(code){
            case "2022":
                year = "2022";
                break;
            case "2023":
                year = "2023";
                break;
            default:
                year = "invalid!";
                break;
        }

        return year;
    }
    private String getSection(String code){
        String section="";
        switch (code){
            case "Ma":
                section = "Morning first shift";
                break;
            case "Mb":
                section = "Morning second shift";
                break;
            case "Aa":
                section = "Afternoon first shift";
                break;
            case "Ab":
                section = "Afternoon second shift";
                break;
            default:
                section = "Invalid!";
                break;
        }

        return section;
    }
}